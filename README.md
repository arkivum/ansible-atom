# ansible-atom

This role helps to install Access to Memory (AtoM) in Linux (trusty, xenial and Centos / RedHat 7).

Please feel free to add support for other platforms, pull requests accepted!

Please visit our [deploy-pub](https://github.com/artefactual/deploy-pub/tree/master/playbooks/atom) repository for a real usage example.

## Notes on dependencies

- AtoM <=2.4, Binder 0.8: Elasticsearch>=1.3,<2.0
-   Atom 2.5, Binder 0.9: Elasticsearch>=5.3,<6.x